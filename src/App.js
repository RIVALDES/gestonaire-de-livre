import logo from './affiche1.png';
import React from 'react'
import './App.css';


class App extends React.Component {


  constructor(props) {
    super(props)

    this.state = {

      password: '',
      userName: '',
      email: '',
      confPassword: ''
    }

  }
   

  render() {
    return (
      <div className="container">
        <h1>Inscription</h1>
        <div className="body">

          <div className="imgContainer">
            <img src={logo} alt="affiche" />
          </div>

          <div className="formContainer">

            <form>
              <div>
                <label>user Name </label>
                <input type="text"  required />
              </div>
              <div>
                <label> Email </label>
                <input type="email" required />
              </div>
              <div>
                <label> Password </label>
                <input type="password" required />
              </div>

              <div>
                <label>Password Confirm </label>
                <input type="password" required />
              </div>
              <div>
                <button type="reset">
                  Annuller
                </button>
                <button type="submit">
                  Inscrire
                </button>
              </div>
            </form>
          </div>


        </div>
      </div>
    );
  }
}

export default App;
