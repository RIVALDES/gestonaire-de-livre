import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from '../Header'
import Login from '../Login'
import ErrorPage from '../ErroPage'
import Home from '../Home'
import '../../App.css';
import SignUp from '../Signup';



function App() {
  return (
    <Router>
      <Header/>
      <Switch>
        <Route exact path="/" component={SignUp} />
        <Route path="/home" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={SignUp} />
        <Route component={ErrorPage} />
      </Switch>

    </Router>
  );
}

export default App;
