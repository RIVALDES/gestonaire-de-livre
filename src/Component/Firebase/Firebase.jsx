import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyDLXmQRPfkA7q3wzj92q8C6nVlyspe8sXs",
    authDomain: "authentification-1.firebaseapp.com",
    projectId: "authentification-1",
    storageBucket: "authentification-1.appspot.com",
    messagingSenderId: "1054525626252",
    appId: "1:1054525626252:web:ab4df3e8900dd140b228a9",
    measurementId: "G-ZVH4N50QRH"
};


class Firebase {
    constructor() {
        app.initializeApp(config);
        this.auth = app.auth();
        this.db = app.firestore()
    }



    // inscription
    signupUser = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    // Connexion

    loginUser = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);


    // Déconnexion
    signoutUser = () => this.auth.signOut();


    // Récupérer le mot de passe
    passwordReset = email => this.auth.sendPasswordResetEmail(email);

    // firestore
    user = uid => this.db.doc(`users/${uid}`);

    //create reader
    createReader = (reader) =>
        this.db.collection('library').doc(reader.id).set(reader);


    //update library 

    updateReader = reader => {
        this.db.collection('library').doc(reader.id).update(reader);
    }


    //get list
    getReaders = () => this.db.collection('library').get();

    //delete Reader
    delleteReader = (id) => this.db.collection('library').doc(id).delete();


}

export default Firebase;