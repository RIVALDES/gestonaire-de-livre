

const FormModal = (props) => {

    return (
        <div className="modal" style={{ display: !props.modal ? 'none' : 'flex' }}>
            <div className="pop">
                <form onSubmit={(e) => props.handleSubmit(e)}>
                    <button onClick={props.hideModale} type="reset">  <span>X</span></button>
                    <div>
                        <label>Titre</label>
                        <input value={props.input.title} onInput={(e) => props.handleChange(e)} name="title" type="text" id="title" required />
                    </div>
                    <div>
                        <label> Auteur </label>
                        <input type="text" value={props.input.auteur} onInput={(e) => props.handleChange(e)} name="auteur" id="auteur" required />
                    </div>
                    <div>
                        <label> Pays </label>
                        <input type="text" value={props.input.pays} onInput={(e) => props.handleChange(e)} name="pays" id="pays" required />
                    </div>

                    <div>
                        <label>Date creation </label>
                        <input type="Date" value={props.input.date} onInput={(e) => props.handleChange(e)} name="date" id="date" required />
                    </div>
                    <div>
                        <button type="reset" onClick={props.hideModale}>
                            Annuller
                        </button>
                        <button type="submit" >
                            Ajouter
                        </button>
                    </div>
                </form>
            </div>

        </div>
    )
}

export default FormModal;