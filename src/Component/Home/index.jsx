import React from "react";
import _uniqueId from 'lodash/uniqueId'
import FormModal from "../FomModal";
import { FirebaseContext } from "../Firebase";

const inputData = {
    title: '',
    auteur: '',
    pays: '',
    date: '',
}

class Home extends React.Component {

    static contextType = FirebaseContext;

    firebase = this.context;

    state = {
        livres: [
        ],
        modal: false,
        input: {
            title: '',
            auteur: '',
            pays: '',
            date: ''

        }
    }

    handleChange = (e) => {
        this.setState({ input: { ...this.state.input, [e.target.id]: e.target.value } })
    }

    componentDidMount() {
       const livres=[];
       this.firebase.getReaders().then((query)=>{
           query.forEach((doc)=>{
               livres.push(doc.data());

           })
           this.setState({livres});
       }).catch((err)=>{
           console.log("error inconue");
       })
       
    }

    Submit = (e) => {
        e.preventDefault();
        const livres = this.state.livres.slice();
        const livre = { ...this.state.input, "id": _uniqueId('livre-') };

        this.firebase.createReader(livre)
            .then(() => { console.log("success") })
            .catch((err) => {
                console.log("error");
            })

        livres.push(livre);
        this.setState({ livres, input: inputData, modal: false });
        e.target.reset();
    }

    handleSubmit = this.Submit;

    handleEdit = (index) => {
        this.setState({ input: this.state.livres[index], modal: true });
        this.handleSubmit = (e) => {
            e.preventDefault();
            const livres = this.state.livres.slice();
            livres[index] = { ...this.state.input };
            this.firebase.createReader(livres[index])
                .then((succe) => { console.log(succe) })
                .catch((err) => {
                    console.log("erro");
                })
            this.setState({ livres, modal: false, input: inputData });
        }
    }


    handleDelete = (index, id) => {
        const livres = this.state.livres.slice();
        livres.splice(index, 1);
        this.firebase.delleteReader(id)
            .then(() => {
                console.log("succes");
            })
            .catch((err) => {
                console.log("erroe")
            })
        this.setState({ livres });
    }

    hideModale = () => {
        this.setState({ modal: false, input: inputData })
    }

    render() {

        const tableRows = this.state.livres.map((elem, index) => {
            return (<tr key={index}>
                <td> {elem.title}</td>
                <td> {elem.auteur}</td>
                <td> {elem.pays}</td>
                <td> {elem.date}</td>
                <td><button onClick={() => this.handleEdit(index, elem.id)} >Editer</button></td>
                <td><button onClick={() => this.handleDelete(index, elem.id)}>Supimer</button></td>
            </tr>)
        })


        return (
            <div className="containerHome">
                <FormModal
                    handleSubmit={this.handleSubmit} setState={this.setState} inputData={inputData} handleChange={this.handleChange}
                    input={this.state.input} hideModale={this.hideModale} modal={this.state.modal}
                />
                <div className="add">
                    <button onClick={() => { this.handleSubmit = this.Submit; this.setState({ modal: true }) }}>+ Ajouter</button>
                </div>
                <article>
                    <table>
                        <thead>
                            <tr>
                                <th> Titre</th>
                                <th> Auteu</th>
                                <th> Pays</th>
                                <th> date</th>
                                <th colSpan={2}>action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableRows}
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </article>

            </div>
        )
    }
}

export default Home;