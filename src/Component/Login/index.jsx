import logo from '../../affiche1.png'
import { Link } from 'react-router-dom'
import { useState, useContext } from 'react';
import { FirebaseContext } from '../Firebase';

const Login = (props) => {

    const firebase = useContext(FirebaseContext);
    const data = {
        email: '',
        password: '',
    }

    const [loginData, setLoginData] = useState(data);
    const [error, setError] = useState('')

    const handleChange = e => {
        setLoginData({ ...loginData, [e.target.id]: e.target.value });
    }

    const handleSubmit = e => {

        e.preventDefault();
        const { email, password } = loginData;
        console.log(loginData);

        firebase.loginUser(email, password)
            .then(authUser => {
                setLoginData({...data});
                props.history.push('/home');
            })
            .catch(error => {
                setError(error);
            })
    }

    const errorMsg = error === '' ? '' :<div className="error"> <span>{error.message}</span></div>
    return (
        <div className="container">
            <h1>Connection</h1>
            <div className="body">

                <div className="imgContainer">
                    <img src={logo} alt="affiche" />
                </div>


                <div className="formContainer" >

                    
                        {errorMsg}
                    
                    <form onSubmit={handleSubmit}>
                        <div>
                            <label> Email </label>
                            <input type="email" onChange={handleChange} id="email" required />
                        </div>
                        <div>
                            <label> Password </label>
                            <input type="password" onChange={handleChange} id="password" required />
                        </div>

                        <div>
                            <button type="reset">
                                Annuller
                            </button>
                            <button type="submit">
                                Inscrire
                            </button>
                        </div>
                    </form>

                    <div className="linkContainer">
                        <Link to="/signup">Vous n'avez pas de compte ? Inscrivez-vous.</Link>
                    </div>
                </div>


            </div>
        </div>
    )
}

export default Login;