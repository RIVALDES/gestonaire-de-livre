import logo from '../../affiche1.png'
import { Link } from 'react-router-dom'
import { useState, useContext } from 'react'
import { FirebaseContext } from '../Firebase'

const SignUp = (props) => {

    const firebase = useContext(FirebaseContext);
    const data = {
        nom: '',
        email: '',
        password: '',
        confirm: ''
    }

    const [loginData, setLoginData] = useState(data);
    const [error, setError] = useState('')

    const handleChange = e => {
        setLoginData({ ...loginData, [e.target.id]: e.target.value });
    }

    const handleSubmit = e => {

        e.preventDefault();
        const { email, password, nom, confirm } = loginData;
        console.log(loginData);



        if (nom.trim().length < 3) {
            setError({ message: "le Nom doit etre plus de 3 caractere" })
            return;
        }


        if (password !== confirm) {
            setError({ message: "le mot de passe  doit etre egale a la confimmation " })
            return;
        }

        firebase.signupUser(email, password)
            .then(authUser => {
                return firebase.user(authUser.user.uid).set({
                    nom,
                    email
                })
            })

            .then(() => {
                setLoginData({ ...data });
                props.history.push('/home');
            })
            .catch(error => {
                setError(error);
            })

    }


    const errorMsg = error === '' ? '' : <div className="error"> <span>{error.message}</span></div>
    return (
        <div className="container">
            <h1>Inscription</h1>
            <div className="body">

                <div className="imgContainer">
                    <img src={logo} alt="affiche" />
                </div>

                <div className="formContainer">

                    {errorMsg}

                    <form onSubmit={handleSubmit}>
                        <div>
                            <label>user Name </label>
                            <input onChange={handleChange} type="text" id="nom" required />
                        </div>
                        <div>
                            <label> Email </label>
                            <input onChange={handleChange} type="email" id="email" required />
                        </div>
                        <div>
                            <label> Password </label>
                            <input onChange={handleChange} type="password" id="password" required />
                        </div>

                        <div>
                            <label>Password Confirm </label>
                            <input onChange={handleChange} type="password" id="confirm" required />
                        </div>
                        <div>
                            <button type="reset">
                                Annuller
                            </button>
                            <button type="submit" >
                                Inscrire
                            </button>
                        </div>
                    </form>
                    <div className="linkContainer">
                        <Link to="/login">Déjà inscrit ? Connectez-vous.</Link>
                    </div>
                </div>


            </div>
        </div>
    )
}

export default SignUp;